# README #

Este documento mostra como usar este simples pacote para envio de mensagens via Slack.

### Como instalar este pacote? ###

Via composer você pode instalar este pacote facilmente.

````
composer require autoaction/messages
````

### Como rodar os testes? ###

````
./bin/phpunit
````

### Icones ###

* :octagonal_sign:
* :warning:
* :black_circle:

### Como usar SIMPLES? ###

````
use AutoAction\Messages\Slack\ClientSlack;

$slack = new ClientSlack([
    'webhook' => 'https://hooks.slack.com/services/SEU-WEBHOOK',
    'user'    => 'hg-quotation',
    'channel' => '#quotation'
]);
$slack->send('minha mensagem');
````

### Como usar mais Avançado? ###

````
use AutoAction\Messages\Slack\ClientSlack;

$slack = new ClientSlack([
    'webhook' => 'https://hooks.slack.com/services/SEU-WEBHOOK',
    'user'    => 'hg-quotation',
    'channel' => '#quotation'
]);


$slack->setChannel('#quotation-alert')->send('minha mensagem');
````
