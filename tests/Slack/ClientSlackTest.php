<?php

namespace Test\AutoAction\Messages\Slack;

use AutoAction\Messages\Slack\ClientSlack;
use AutoAction\Messages\Slack\SlackEnum;
use Exception;
use PHPUnit\Framework\TestCase;

class ClientSlackTest extends TestCase
{
    private $webhook = 'http://my-webhook';
    private $server = 'https://my-server.com';

    public function testValoresPadroesDeConfiguracao()
    {
        $clientSlack = new ClientSlack();
        $this->assertEquals(SlackEnum::MESSAGE_TYPE_NOTICE, $clientSlack->getType());
        $this->assertEquals(SlackEnum::MESSAGE_ICON_NOTICE, $clientSlack->getIcon());
        $this->assertEquals('undefined', $clientSlack->getUser());
    }

    /**
     * @expectedException Exception
     */
    public function testExceptionWebhook()
    {
        $clientSlack = new ClientSlack();
        $clientSlack->getWebhook();
    }

    /**
     * @expectedException Exception
     */
    public function testExceptionChannel()
    {
        $clientSlack = new ClientSlack();
        $clientSlack->getChannel();
    }

    /**
     * @expectedException Exception
     */
    public function testExceptionMessage()
    {
        $clientSlack = new ClientSlack();
        $clientSlack->getMessage();
    }

    private function clientSlack()
    {
        return new ClientSlack([
            'webhook' => $this->webhook,
            'type'    => SlackEnum::MESSAGE_TYPE_ERROR,
            'icon'    => SlackEnum::MESSAGE_ICON_ERROR,
            'server'  => $this->server,
            'channel' => 'quotation',
        ]);
    }

    public function testDefinicaoDeConfiguracao()
    {
        $clientSlack = $this->clientSlack();

        $this->assertEquals($this->webhook, $clientSlack->getWebhook(), 'setWebhook');
        $this->assertEquals(SlackEnum::MESSAGE_TYPE_ERROR, $clientSlack->getType(), 'setMessageType');
        $this->assertEquals(SlackEnum::MESSAGE_ICON_ERROR, $clientSlack->getIcon(), 'setMessageIcon');
        $this->assertEquals($this->server, $clientSlack->getServer(), 'setServerHttp');
    }

    public function testAlteracaoDeServerECanal()
    {
        $channel = '@fernando.petry';
        $user = 'hgSystem';

        $clientSlack = $this->clientSlack();
        $clientSlack->setChannel($channel);
        $clientSlack->setIcon(SlackEnum::MESSAGE_ICON_WARNING);
        $clientSlack->setUser($user);

        $this->assertEquals($channel, $clientSlack->getChannel(), 'setMessageType');
        $this->assertEquals(SlackEnum::MESSAGE_ICON_WARNING, $clientSlack->getIcon(), 'setMessageIconUpdate');
        $this->assertEquals($user, $clientSlack->getUser(), 'setMessageUserUpdate');
    }


    public function testMontagemDoPayload()
    {
        $text = 'minha mensagem';
        $icon = 'my-icon';
        $user = 'username';
        $channel = 'my-channel';

        $client = new ClientSlack([
            'message' => $text,
            'icon'    => $icon,
            'user'    => $user,
            'channel' => $channel
        ]);

        $expected = [
            'payload' => json_encode(
                [
                    'text'       => $text,
                    'icon_emoji' => $icon, // :beetle:
                    'username'   => $user,
                    'channel'    => $channel, //and a Direct Message with "channel": "@username".
                    'mrkdwn'     => true,
                ]
            )
        ];

        $this->assertEquals($expected, $client->getPayload());
    }
}
