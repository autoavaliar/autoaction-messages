<?php
declare(strict_types=1);

namespace AutoAction\Messages\Slack;

/**
 * Enum Slack Message
 *
 * @package AutoAction\Messages\Slack
 * @date    12/02/2020 07:52
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class SlackEnum
{
    const SERVER_UNDEFINED = 'https://undefined';

    const MESSAGE_TYPE_NOTICE = 'NOTICE';
    const MESSAGE_TYPE_ERROR = 'ERROR';
    const MESSAGE_TYPE_WARNING = 'WARNING';

    const MESSAGE_ICON_NOTICE = ':black_circle:';
    const MESSAGE_ICON_ERROR = ':octagonal_sign:';
    const MESSAGE_ICON_WARNING = ':warning:';
}