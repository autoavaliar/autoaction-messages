<?php
declare(strict_types=1);

namespace AutoAction\Messages\Slack;

use Exception;
use GuzzleHttp\Client;

/**
 * Client Slack Message
 *
 * @package AutoAction\Messages\Slack
 * @date    12/02/2020 07:46
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class ClientSlack
{
    private $serverHttp;
    private $messageType = SlackEnum::MESSAGE_TYPE_NOTICE;
    private $messageIcon = SlackEnum::MESSAGE_ICON_NOTICE;
    private $slackWebhook;
    private $user;
    private $channel;
    private $message;

    /**
     * @param array $config Configurações do Client Slack
     *
     * @throws Exception
     */
    public function __construct(array $config = [])
    {
        if (isset($config['webhook'])) {
            $this->setWebhook($config['webhook']);
        }

        if (isset($config['type'])) {
            $this->setType($config['type']);
        }

        if (isset($config['icon'])) {
            $this->setIcon($config['icon']);
        }

        if (isset($config['server'])) {
            $this->setServer($config['server']);
        }

        if (isset($config['channel'])) {
            $this->setChannel($config['channel']);
        }

        if (isset($config['message'])) {
            $this->setMessage($config['message']);
        }

        if (isset($config['user'])) {
            $this->setUser($config['user']);
        }
    }

    private function setServer(string $serverHttp)
    {
        $this->serverHttp = $serverHttp;
        return $this;
    }

    private function setType(string $messageType)
    {
        $this->messageType = $messageType;
        return $this;
    }

    public function setIcon(string $messageIcon)
    {
        if (empty($messageIcon) && is_null($this->messageIcon)) {
            $this->messageIcon = SlackEnum::MESSAGE_ICON_NOTICE;
        }


        $this->messageIcon = $messageIcon;
        return $this;
    }

    public function setUser(string $user)
    {
        $this->user = $user;
        return $this;
    }


    public function setWebhook(string $slackWebhook)
    {
        $this->slackWebhook = $slackWebhook;
        return $this;
    }

    public function setChannel(string $channel)
    {
        $this->channel = $channel;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getServer(): string
    {
        return $this->serverHttp;
    }

    public function getType(): string
    {
        return $this->messageType;
    }

    public function getIcon(): string
    {
        return $this->messageIcon;
    }

    public function getUser(): string
    {
        if (is_null($this->user) || empty($this->user)) {
            $this->user = 'undefined';
        }

        return $this->user;
    }

    public function getWebhook(): string
    {
        if (is_null($this->channel) || empty($this->channel)) {
            throw new Exception('Webhook configuration not found! Enter the parameter [webhook].');
        }

        return $this->slackWebhook;
    }

    public function getChannel(): string
    {
        if (is_null($this->channel) || empty($this->channel)) {
            throw new Exception('Destination configuration not found! Enter the parameter [channel].');
        }

        return $this->channel;
    }

    public function getMessage(): string
    {
        if (is_null($this->message) || empty($this->message)) {
            throw new Exception('Message configuration not found! Enter the parameter [message].');
        }

        return $this->message;
    }

    public function getPayload(): array
    {
        return [
            'payload' => json_encode(
                [
                    'text'       => $this->getMessage(),
                    'icon_emoji' => $this->getIcon(), // :beetle:
                    'username'   => $this->getUser(),
                    'channel'    => $this->getChannel(), //and a Direct Message with "channel": "@username".
                    'mrkdwn'     => true,
                ]
            )
        ];
    }

    public function send(string $message)
    {
        $this->setMessage($message);

        $client = new Client();
        return $client->request('POST', $this->getWebhook(), [
            'form_params' => $this->getPayload()
        ]);
    }

}